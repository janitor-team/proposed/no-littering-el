Source: no-littering-el
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-elpa
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/emacscollective/no-littering
Vcs-Browser: https://salsa.debian.org/emacsen-team/no-littering-el
Vcs-Git: https://salsa.debian.org/emacsen-team/no-littering-el.git

Package: elpa-no-littering
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: help keeping ~/.emacs.d clean
 The default paths used to store configuration files and persistent
 data are not consistent across Emacs packages. This isn't just a
 problem with third-party packages but even with built-in packages.
 .
 This package sets out to fix this by changing the values of path
 variables to put files in either `no-littering-etc-directory'
 (defaulting to "~/.emacs.d/etc/") or `no-littering-var-directory'
 (defaulting to "~/.emacs.d/var/"), and by using descriptive file
 names and subdirectories when appropriate.
